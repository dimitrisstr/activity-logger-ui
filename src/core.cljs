(ns core
  (:require
   [reagent.dom :as rdom]
   [re-frame.core :as rf]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [api.events]
   [api.subs]
   [config]
   [dashboard.views]
   [events]
   [log.views]
   [utilities]))

(def default-db
  {:tab :home
   :sessions []
   :dashboard-tab :activities})

(rf/reg-event-db
 ::initialize-db
 (fn-traced [_ _]
            default-db))

(rf/reg-sub
 ::tab
 (fn [db]
   (:tab db)))

(rf/reg-event-db
 ::set-tab
 (fn [db [_ tab]]
   (assoc db :tab tab)))

(defn dev-setup []
  (when config/debug?
    (println "dev mode")))

(defn main-panel []
  (let [current-tab @(rf/subscribe [::tab])
        activities-map @(rf/subscribe [::api.subs/activities])]
    (doseq [activity-id (keys activities-map)]
      (rf/dispatch [::api.events/get-activity-tags activity-id]))
    [:div
     [:div.hero.is-primary
      [:div.hero-body
       [:p.title "Activity Logger"]]]
     [:div.tabs.is-centered.is-large
      [:ul
       [:li (when (= current-tab :home) {:class "is-active"})
        [:a {:on-click #(rf/dispatch [::set-tab :home])} "Home"]]
       [:li (when (= current-tab :log) {:class "is-active"})
        [:a {:on-click #(rf/dispatch [::set-tab :log])} "Log"]]]]
     (cond
       (= current-tab :home) [dashboard.views/dashboard-panel]
       (= current-tab :log) [log.views/log-panel])]))

(defn ^:dev/after-load mount-root []
  (rf/clear-subscription-cache!)
  (let [root-el (.getElementById js/document "app")]
    (rdom/unmount-component-at-node root-el)
    (rdom/render [main-panel] root-el)))

(defn init []
  (rf/dispatch-sync [::initialize-db])
  (rf/dispatch-sync [::api.events/get-tags])
  (rf/dispatch-sync [::api.events/get-activities])
  (rf/dispatch-sync [::api.events/get-sessions])
  (dev-setup)
  (mount-root))
