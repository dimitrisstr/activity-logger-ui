(ns log.views
  (:require
   [reagent.core :as re]
   [re-frame.core :as rf]
   [api.events]
   [api.subs]
   [log.events :as events]
   [log.subs :as subs]))

(defn modal-window [component close-callback show-modal?]
  [:div.modal {:class (when show-modal? "is-active")}
   [:div.modal-background]
   [:div.modal-content
    [:div.box
     [component]]]
   [:button.modal-close.is-large {:aria-label "close"
                                  :on-click close-callback}]])

(defn crud-buttons [new-callback update-callback delete-callback disabled?]
  [:div
   [:div.field [:button.button.is-primary {:type "submit"
                                           :on-click new-callback} "New"]]
   [:div.field [:button.button.is-primary {:type "submit"
                                           :on-click update-callback
                                           :disabled disabled?} "Edit"]]
   [:div.field [:button.button.is-danger {:type "submit"
                                          :on-click delete-callback
                                          :disabled disabled?} "Delete"]]])

(defn input-field [label component]
  [:div.field
   [:label.field-label label]
   [:div.control
    component]])

;;;; Tags panel

(defn tags-table [tags]
  (let [selected-tag @(rf/subscribe [::subs/selected :tag])]
    [:div
     [:table.table.is-hoverable
      [:thead
       [:tr
        [:th "Name"]
        [:th "Created At"]]]
      [:tbody
       (for [tag tags]
         ^{:key tag}
         [:tr {:class (when (= (:tag_id tag) (:tag_id selected-tag)) "is-selected")
               :on-click #(rf/dispatch [::events/set-selected :tag tag])}
          [:td (:name tag)]
          [:td (.toDateString (:created_at tag))]])]]]))

(defn tags-crud-buttons []
  (let [selected-tag @(rf/subscribe [::subs/selected :tag])]
    [crud-buttons
     #(rf/dispatch [::events/toggle-modal :create-tag])
     #(rf/dispatch [::events/toggle-modal :update-tag])
     #(rf/dispatch [::api.events/delete-tag (:tag_id selected-tag)])
     (not selected-tag)]))

(defn tag-form [on-submit input-tag]
  (let [form-data  (re/atom input-tag)]
    (fn []
      (let [{:keys [name] :as tag} @form-data]
        [:div
         [input-field "Name"
          [:input.input {:type "text"
                         :id "name"
                         :value name
                         :placeholder "Name"
                         :on-change #(swap! form-data assoc :name (-> % .-target .-value))}]]
         [:div.field
          [:div.control
           [:button {:class "button is-primary"
                     :type "submit"
                     :on-click #(on-submit tag)} "Submit"]]]]))))

(defn create-tag-on-submit [tag]
  (rf/dispatch [::api.events/insert-tag tag])
  (rf/dispatch [::events/toggle-modal :create-tag]))

(defn update-tag-on-submit [tag]
  (rf/dispatch [::api.events/update-tag (:tag_id tag) tag])
  (rf/dispatch [::events/toggle-modal :update-tag]))

(defn create-tag-modal []
  (let [show-modal @(rf/subscribe [::subs/show-modal? :create-tag])]
    [modal-window
     #(tag-form create-tag-on-submit {})
     #(rf/dispatch [::events/toggle-modal :create-tag])
     show-modal]))

(defn update-tag-modal []
  (let [show-modal @(rf/subscribe [::subs/show-modal? :update-tag])
        selected-tag @(rf/subscribe [::subs/selected :tag])]
    [modal-window
     #(tag-form update-tag-on-submit selected-tag)
     #(rf/dispatch [::events/toggle-modal :update-tag])
     show-modal]))

(defn tags-panel []
  (let [values @(rf/subscribe [::subs/sorted-tags :name])]
    [:div.container
     [:div.columns.is-centered
      [:div.column.is-narrow
       [tags-table values]]
      [:div.column.is-narrow
       [tags-crud-buttons]]
      [create-tag-modal]
      [update-tag-modal]]]))

;;;; Activities panel

(defn activities-table [activities]
  (let [selected-activity @(rf/subscribe [::subs/selected :activity])]
    [:div
     [:table.table.is-hoverable
      [:thead
       [:tr
        [:th "Name"]
        [:th "Description"]
        [:th "Created At"]
        [:th "Completed"]]]
      [:tbody
       (for [activity activities]
         ^{:key activity}
         [:tr {:class (when (= (:activity_id activity) (:activity_id selected-activity)) "is-selected")
               :on-click #(rf/dispatch [::events/set-selected :activity activity])}
          [:td (:name activity)]
          [:td (:description activity)]
          [:td (.toDateString (:created_at activity))]
          [:td (str (:completed activity))]])]]]))

(defn activities-crud-buttons []
  (let [selected-activity @(rf/subscribe [::subs/selected :activity])]
    [crud-buttons
     #(rf/dispatch [::events/toggle-modal :create-activity])
     #(rf/dispatch [::events/toggle-modal :update-activity])
     #(rf/dispatch [::api.events/delete-activity (:activity_id selected-activity)])
     (not selected-activity)]))

(defn activity-form [on-submit input-activity]
  (let [form-data  (re/atom input-activity)]
    (fn []
      (let [{:keys [name description completed] :as activity} @form-data]
        [:div
         [input-field "Name"
          [:input.input {:type "text"
                         :id "name"
                         :value name
                         :placeholder "Name"
                         :on-change #(swap! form-data assoc :name (-> % .-target .-value))}]]
         [input-field "Description"
          [:input.input {:type "text"
                         :id "description"
                         :value description
                         :placeholder "Description"
                         :on-change #(swap! form-data assoc :description (-> % .-target .-value))}]]
         [input-field "Completed"
          [:div.select
           [:select {:id "completed"
                     :defaultValue ""
                     :on-change #(swap! form-data assoc :completed (-> % .-target .-value (= "true")))}
            [:option {:hidden true :disabled true :value ""} "Select"]
            [:option {:value true} "Yes"]
            [:option {:value false} "No"]]]]
         [:div.field
          [:div.control
           [:button {:class "button is-primary"
                     :type "submit"
                     :on-click #(on-submit activity)} "Submit"]]]]))))

(defn create-activity-on-submit [activity]
  (rf/dispatch [::api.events/insert-activity activity])
  (rf/dispatch [::events/toggle-modal :create-activity]))

(defn update-activity-on-submit [activity]
  (rf/dispatch [::api.events/update-activity (:activity_id activity) activity])
  (rf/dispatch [::events/toggle-modal :update-activity]))

(defn create-activity-modal []
  (let [show-modal @(rf/subscribe [::subs/show-modal? :create-activity])]
    [modal-window
     #(activity-form create-activity-on-submit {})
     #(rf/dispatch [::events/toggle-modal :create-activity])
     show-modal]))

(defn update-activity-modal []
  (let [show-modal @(rf/subscribe [::subs/show-modal? :update-activity])
        selected-activity @(rf/subscribe [::subs/selected :activity])]
    [modal-window
     #(activity-form update-activity-on-submit selected-activity)
     #(rf/dispatch [::events/toggle-modal :update-activity])
     show-modal]))

(defn activities-panel []
  (let [activities @(rf/subscribe [::subs/sorted-activities :name])]
    [:div.container
     [:div.columns.is-centered
      [:div.column.is-narrow
       [activities-table activities]]
      [:div.column.is-narrow
       [activities-crud-buttons]]
      [create-activity-modal]
      [update-activity-modal]]]))

;;;; Sessions panel

(defn sessions-table [sessions]
  (let [selected-session @(rf/subscribe [::subs/selected :session])]
    [:div
     [:table.table.is-hoverable
      [:thead
       [:tr
        [:th "Day"]
        [:th "Duration"]]]
      [:tbody
       (for [session sessions]
         ^{:key session}
         [:tr {:class (when (= (:session_id session) (:session_id selected-session)) "is-selected")
               :on-click #(rf/dispatch [::events/set-selected :session session])}
          [:td (.toDateString (:day session))]
          [:td (:duration session)]])]]]))

(defn sessions-crud-buttons []
  (let [selected-session @(rf/subscribe [::subs/selected :session])]
    [crud-buttons
     #(rf/dispatch [::events/toggle-modal :create-session])
     #(rf/dispatch [::events/toggle-modal :update-session])
     #(rf/dispatch [::api.events/delete-session (:session_id selected-session)])
     (not selected-session)]))

(defn session-form [on-submit input-session]
  (let [form-data (re/atom input-session)]
    (fn []
      (let [{:keys [_day duration] :as session} @form-data]
        [:div
         [input-field "Day"
          [:input.input {:type "text"
                         :id "day"
                         :placeholder "Day"
                         :on-change #(swap! form-data assoc :day (-> % .-target .-value))}]]
         [input-field "Duration"
          [:input.input {:type "text"
                         :id "duration"
                         :value duration
                         :placeholder "Description"
                         :on-change #(swap! form-data assoc :duration (-> % .-target .-value))}]]
         [:div.field
          [:div.control
           [:button {:class "button is-primary"
                     :type "submit"
                     :on-click #(on-submit session)} "Submit"]]]]))))

(defn create-session-on-submit [session]
  (let [selected-activity @(rf/subscribe [::subs/selected :session-activity])]
    (rf/dispatch [::api.events/insert-session selected-activity session])
    (rf/dispatch [::events/toggle-modal :create-session])))

(defn update-session-on-submit [session]
  (rf/dispatch [::api.events/update-session (:session_id session) session])
  (rf/dispatch [::events/toggle-modal :update-session]))

(defn create-session-modal []
  (let [show-modal @(rf/subscribe [::subs/show-modal? :create-session])]
    [modal-window
     #(session-form create-session-on-submit {})
     #(rf/dispatch [::events/toggle-modal :create-session])
     show-modal]))

(defn update-session-modal []
  (let [show-modal @(rf/subscribe [::subs/show-modal? :update-session])
        selected-session @(rf/subscribe [::subs/selected :session])]
    [modal-window
     #(session-form update-session-on-submit selected-session)
     #(rf/dispatch [::events/toggle-modal :update-session])
     show-modal]))

(defn sessions-panel []
  (let [activities @(rf/subscribe [::subs/sorted-activities :name])
        incomplete-activities (filter #(not (:completed %)) activities)
        selected-activity @(rf/subscribe [::subs/selected :session-activity])]
    [:div.container
     [:div.columns.is-centered
      [:div.column.is-narrow
       [:div.select
        [:select {:id "activities"
                  :defaultValue ""
                  :on-change #(do
                                (rf/dispatch [::events/set-selected :session nil])
                                (rf/dispatch [::events/set-selected :session-activity (-> % .-target .-value js/parseInt)]))}
         [:option {:hidden true :disabled true :value ""} "Select"]
         (map #(vec [:option {:value (:activity_id %) :key (:activity_id %)} (:name %)]) incomplete-activities)]]]]
     [:div.columns.is-centered
      [:div.column.is-narrow
       (when (some? selected-activity)
         [:div.columns.is-centered
          [:div.column.is-narrow
           [sessions-table @(rf/subscribe [::subs/sessions-by-activity selected-activity])]]
          [:div.column.is-narrow
           [sessions-crud-buttons]]
          [create-session-modal]
          [update-session-modal]])]]]))

(defn tabs []
  (let [current-tab @(rf/subscribe [::subs/tab])]
    [:div.tabs.is-centered.is-toggle
     [:ul
      [:li {:class (when (= current-tab :tags) "is-active")}
       [:a {:on-click #(rf/dispatch [::events/set-tab :tags])} "Tags"]]
      [:li {:class (when (= current-tab :sessions) "is-active")}
       [:a {:on-click #(rf/dispatch [::events/set-tab :sessions])} "Sessions"]]
      [:li {:class (when (= current-tab :activities) "is-active")}
       [:a {:on-click #(rf/dispatch [::events/set-tab :activities])} "Activities"]]]]))

(defn tab-panel []
  (let [current-tab @(rf/subscribe [::subs/tab])]
    (cond
      (= current-tab :tags) [tags-panel]
      (= current-tab :activities) [activities-panel]
      (= current-tab :sessions) [sessions-panel])))

(defn log-panel []
  (rf/dispatch [::events/set-tab :tags])
  [:div
   [tabs]
   [tab-panel]])
