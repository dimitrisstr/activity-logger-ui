(ns log.subs
  (:require
   [re-frame.core :as rf]))

(rf/reg-sub
 ::tab
 (fn [db]
   (:log-tab db)))

(rf/reg-sub
 ::selected
 (fn [db [_ key]]
   (get-in db [:selected key])))

(rf/reg-sub
 ::show-modal?
 (fn [db [_ modal-key]]
   (get-in db [:show-modal modal-key])))

(rf/reg-sub
 ::sessions-by-activity
 (fn [db [_ activity-id]]
   (->> (:sessions db)
        vals
        flatten
        (filter #(= (:activity_id %) activity-id))
        (sort-by :day))))

(rf/reg-sub
 ::sorted-tags
 (fn [db [_ key]]
   (->> db :tags vals (sort-by key))))


(rf/reg-sub
 ::sorted-activities
 (fn [db [_ key]]
   (->> db :activities vals (sort-by key))))
