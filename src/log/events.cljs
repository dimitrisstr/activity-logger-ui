(ns log.events
  (:require
   [re-frame.core :as rf]))

(rf/reg-event-db
 ::set-tab
 (fn [db [_ new-route]]
   (-> db
       (assoc :log-tab new-route)
       (assoc :selected nil))))

(rf/reg-event-db
 ::set-selected
 (fn [db [_ key entity]]
   (assoc-in db [:selected key] entity)))

(rf/reg-event-db
 ::toggle-modal
 (fn [db [_ modal-key]]
   (update-in db [:show-modal modal-key] not)))
