(ns utilities
  (:require
   [clojure.string :as str]))

(defn pad-zero [num]
  (if (< num 10)
    (str "0" num)
    (str num)))

(defn str-date->js-date
  "Converts a date string to a JavaScript Date object.
   
   The date string should be in the format YYYY-MM-DDTHH:mm:ss'
   
   Examples:
    (str-date->js-date \"2023-05-29T10:30:00\")"
  [date-str]
  (js/Date. (js/Date.parse date-str)))

(defn js-date->str-date
  "Converts a javascript Date object to a date string.
   
   The format of the date string is YYYY-MM-DD."
  [js-date]
  (str (.getFullYear js-date) "-"
       (pad-zero (inc (.getMonth js-date))) "-"
       (pad-zero (.getDate js-date))))

(defn js-date->int-date
  "Converts a javescript Date object to a yyyyMMdd number."
  [js-date]
  (+ (-> js-date .getFullYear (* 10000))
     (-> js-date .getMonth inc (* 100))
     (.getDate js-date)))

(defn int-date->str-date
  [int-date]
  (let [year (quot int-date 10000)
        month (quot (rem int-date 10000) 100)
        day (rem int-date 100)]
    (str year "-" (pad-zero month) "-" (pad-zero day))))

(defn time-str->float
  "Converts a time string to a float.
   
   The time string should be in the format HH:mm
   
   Notes:
   The float number is rounded to 1 decimal place."
  [time-str]
  (let [[hours minutes _] (map #(js/parseInt %) (str/split time-str #":"))
        time-float (+ hours (/ minutes 60))]
    (/ (Math/round (* time-float 10)) 10)))

(defn time-str->time-str-pp
  "Converts a time string to a pretty print time string.
   
   The time string should be in the format HH:mm.
   
   Examples:
   (time-str->time-str-pp \"03:50\")
   The output will be 3h 50m"
  [time-str]
  (let [[hours minutes _] (map #(js/parseInt %) (str/split time-str #":"))]
    (str (when (pos? hours) (str hours "h "))
         (when (pos? minutes) (str minutes "m ")))))

(defn start-end-of-week
  "Computes the start and end dates of the week for the given JavaScript Date object.
   
   Returns a map containing the start and end dates of the week. 
   
   Notes:
   The first day of the week is Monday and the last day is Sunday."
  [js-date]
  (let [start-of-week (js/Date.
                       (js/Date.UTC
                        (.getFullYear js-date)
                        (.getMonth js-date)
                        (- (.getDate js-date) (.getDay js-date) (if (zero? (.getDay js-date)) 6 -1))))
        end-of-week (js/Date.
                     (js/Date.UTC
                      (.getFullYear start-of-week)
                      (.getMonth start-of-week)
                      (+ (.getDate start-of-week) 6)))]
    {:start-date start-of-week
     :end-date end-of-week}))

(defn start-end-of-month
  "Computes the start and end dates of the month for the given JavaScript Date object.
   
   Returns a map containing the start and end dates of the month."
  [js-date]
  {:start-date (js/Date.
                (js/Date.UTC
                 (.getFullYear js-date)
                 (.getMonth js-date)
                 1))
   :end-date (js/Date.
              (js/Date.UTC
               (.getFullYear js-date)
               (inc (.getMonth js-date))
               0))})

(defn add-days-to-date
  "Adds the specified number of days to a Javascript Date and returns a new
   Javascript Date object."
  [js-date days]
  (js/Date. (js/Date.UTC
             (.getFullYear js-date)
             (.getMonth js-date)
             (+ (.getDate js-date) days))))

(defn date-diff
  "Calculates the difference in days between two JavaScript Date objects.
   
  Returns the difference in days rounded down to the nearest whole number."
  [js-date1 js-date2]
  (let [time-diff (- (.getTime js-date2) (.getTime js-date1))
        day-time (* 1000 3600 24)]
    (-> (/ time-diff day-time) Math/floor Math/abs)))
