(ns api.subs
  (:require
   [re-frame.core :as rf]))

(rf/reg-sub
 ::tags
 (fn [db]
   (:tags db)))

(rf/reg-sub
 ::activities
 (fn [db]
   (:activities db)))

(rf/reg-sub
 ::sessions
 (fn [db]
   (:sessions db)))
