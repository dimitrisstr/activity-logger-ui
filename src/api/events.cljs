(ns api.events
  (:require
   [clojure.string :as str]
   [ajax.core :as ajax]
   [re-frame.core :as rf]
   [day8.re-frame.http-fx]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [utilities :refer [str-date->js-date js-date->int-date]]))

(def api-url "http://127.0.0.1:3000/api")

(defn endpoint
  "Concat any params to api-url separated by /"
  [& params]
  (str/join "/" (cons api-url params)))

;; GET Tags @ /api/tags -------------------------------------------------------
(rf/reg-event-fx
 ::get-tags
 (fn-traced
  [{db :db} [_ params]]
  {:http-xhrio {:method          :get
                :uri             (endpoint "tags")
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::get-tags-success]
                :on-failure      [::api-request-error ::get-tags]}}))

(rf/reg-event-db
 ::get-tags-success
 (fn-traced [db [_ response]]
            (->> (:results response)
                 (map #(update % :created_at str-date->js-date))
                 (reduce (fn [acc item]
                           (assoc acc (:tag_id item) item)) {})
                 (assoc db :tags))))

;; -- POST Tag @ /api/tags ----------------------------------------------------
(rf/reg-event-fx
 ::insert-tag
 (fn-traced
  [{db :db} [_ params]]
  {:http-xhrio {:method          :post
                :uri             (endpoint "tags")
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::insert-tag-success]
                :on-failure      [::api-request-error ::insert-tag]}}))

(rf/reg-event-db
 ::insert-tag-success
 (fn-traced [db [_ response]]
            (let [tag (update response :created_at str-date->js-date)]
              (-> db
                  (assoc-in [:tags (:tag_id tag)] tag)
                  (assoc-in [:selected :tag] nil)))))

;; -- PUT Tag @ /api/tags -----------------------------------------------------
(rf/reg-event-fx
 ::update-tag
 (fn-traced
  [{db :db} [_ id params]]
  {:http-xhrio {:method          :put
                :uri             (endpoint "tags" id)
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::update-tag-success id]
                :on-failure      [::api-request-error ::update-tag]}}))

(rf/reg-event-db
 ::update-tag-success
 (fn-traced [db [_ id response]]
            (let [tag (update response :created_at str-date->js-date)]
              (-> db
                  (assoc-in [:tags (:tag_id tag)] tag)
                  (assoc-in [:selected :tag] nil)))))

;; -- DELETE Tag @ /api/tags/:id ----------------------------------------------
(rf/reg-event-fx
 ::delete-tag
 (fn-traced [db [_ id]]
            {:http-xhrio {:method          :delete
                          :uri             (endpoint "tags" id)
                          :body            {}
                          :format          (ajax/json-request-format)
                          :response-format (ajax/json-response-format {:keywords? true})
                          :on-success      [::delete-tag-success id]
                          :on-failure      [::api-request-error ::delete-tag]}}))

(rf/reg-event-db
 ::delete-tag-success
 (fn [db [_ id _]]
   (-> db
       (update :tags dissoc id)
       (assoc-in [:selected :tag] nil))))

;; GET Activities @ /api/activities -------------------------------------------
(rf/reg-event-fx
 ::get-activities
 (fn-traced
  [{db :db} [_ params]]
  {:http-xhrio {:method          :get
                :uri             (endpoint "activities")
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::get-activities-success]
                :on-failure      [::api-request-error ::get-activities]}}))

(rf/reg-event-db
 ::get-activities-success
 (fn-traced [db [_ response]]
            (->> (:results response)
                 (map #(update % :created_at str-date->js-date))
                 (reduce (fn [acc item]
                           (assoc acc (:activity_id item) item)) {})
                 (assoc db :activities))))

;; -- POST Activity @ /api/activities -----------------------------------------
(rf/reg-event-fx
 ::insert-activity
 (fn-traced
  [{db :db} [_ params]]
  {:http-xhrio {:method          :post
                :uri             (endpoint "activities")
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::insert-activity-success]
                :on-failure      [::api-request-error ::insert-activity]}}))

(rf/reg-event-db
 ::insert-activity-success
 (fn-traced [db [_ response]]
            (let [activity (update response :created_at str-date->js-date)]
              (-> db
                  (assoc-in [:activities (:activity_id activity)] activity)
                  (assoc-in [:selected :activity] nil)))))

;; -- PUT Activity @ /api/activities ------------------------------------------
(rf/reg-event-fx
 ::update-activity
 (fn-traced
  [{db :db} [_ id params]]
  {:http-xhrio {:method          :put
                :uri             (endpoint "activities" id)
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::update-activity-success id]
                :on-failure      [::api-request-error ::update-activity]}}))

(rf/reg-event-db
 ::update-activity-success
 (fn-traced [db [_ id response]]
            (let [activity (update response :created_at str-date->js-date)]
              (-> db
                  (assoc-in [:activities (:activity_id activity)] activity)
                  (assoc-in [:selected :activity] nil)))))

;; -- DELETE Activity @ /api/activities/:id -----------------------------------
(rf/reg-event-fx
 ::delete-activity
 (fn-traced [db [_ id]]
            {:http-xhrio {:method          :delete
                          :uri             (endpoint "activities" id)
                          :body            {}
                          :format          (ajax/json-request-format)
                          :response-format (ajax/json-response-format {:keywords? true})
                          :on-success      [::delete-activity-success id]
                          :on-failure      [::api-request-error ::delete-activity]}}))

(rf/reg-event-db
 ::delete-activity-success
 (fn [db [_ id _]]
   (-> db
       (update :activities dissoc id)
       (assoc-in [:selected :activity] nil))))

;;-- GET Activity Tags @ /api/activities/:id/tags ----------------------------
(rf/reg-event-fx
 ::get-activity-tags
 (fn-traced
  [{db :db} [_ id params]]
  {:http-xhrio {:method          :get
                :uri             (endpoint "activities" id "tags")
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::get-activity-tags-success id]
                :on-failure      [::api-request-error ::get-activity-tags]}}))

(rf/reg-event-db
 ::get-activity-tags-success
 (fn [db [_ id response]]
   (let [tags (:results response)]
     (assoc-in db [:activities id :tags] tags))))

;; -- PUT Activity Tags @ /api/activities/:id/tags ----------------------------
(rf/reg-event-fx
 ::tag-activity
 (fn-traced
  [{db :db} [_ id params]]
  {:http-xhrio {:method          :put
                :uri             (endpoint "activities" id "tags")
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::tag-activity-success id]
                :on-failure      [::api-request-error ::tag-activity]}}))

;; (rf/reg-event-db
;;  ::tag-activity-success
;;  (fn [db [_ id response]]
;;    (let [tags (:results response)]
;;      (assoc-in db [:activities id :tags] tags))))



;; GET Sessions @ /api/sessions
(rf/reg-event-fx
 ::get-sessions
 (fn-traced
  [{db :db} [_ params]]
  {:http-xhrio {:method          :get
                :uri             (endpoint "sessions")
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::get-sessions-success]
                :on-failure      [::api-request-error ::get-sessions]}}))

(rf/reg-event-fx
 ::get-sessions-by-activity
 (fn-traced
  [{db :db} [_ activity-id params]]
  {:http-xhrio {:method          :get
                :uri             (endpoint "activities" activity-id "sessions")
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::get-sessions-success]
                :on-failure      [::api-request-error ::get-sessions-by-activity]}}))

(rf/reg-event-db
 ::get-sessions-success
 (fn [db [_ response]]
   (->> (:results response)
        (reduce (fn [acc item]
                  (let [updated-item (update item :day str-date->js-date)
                        int-date (js-date->int-date (:day updated-item))]
                    (assoc! acc int-date (conj (get acc int-date []) updated-item))))
                (transient {}))
        (persistent!)
        (assoc db :sessions))))

;; -- POST Session @ /api/activities/:activity_id/sessions --------------------
(rf/reg-event-fx
 ::insert-session
 (fn-traced
  [{db :db} [_ activity-id params]]
  {:http-xhrio {:method          :post
                :uri             (endpoint "activities" activity-id "sessions")
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::insert-session-success]
                :on-failure      [::api-request-error ::insert-session]}}))

(rf/reg-event-db
 ::insert-session-success
 (fn-traced [db [_ response]]
            (let [session (update response :day str-date->js-date)
                  int-date (js-date->int-date (:day session))]
              (-> db
                  (update-in [:sessions int-date] conj session)
                  (assoc-in [:selected :session] nil)))))

;; -- PUT Session @ /api/sessions/:id -----------------------------------------
(rf/reg-event-fx
 ::update-session
 (fn-traced
  [{db :db} [_ id params]]
  {:http-xhrio {:method          :put
                :uri             (endpoint "sessions" id)
                :params          params
                :timeout         80000
                :format          (ajax/json-request-format)
                :response-format (ajax/json-response-format {:keywords? true})
                :on-success      [::update-session-success id]
                :on-failure      [::api-request-error ::update-session]}}))

(rf/reg-event-db
 ::update-session-success
 (fn-traced [db [_ session-id response]]
            (let [session (update response :day str-date->js-date)
                  session-date (->> db
                                    :sessions
                                    vals
                                    flatten
                                    (filter #(= (:session_id %) session-id))
                                    first
                                    :day
                                    js-date->int-date)
                  updated-sessions  (->> (get-in db [:sessions session-date])
                                         (remove #(= (:session_id %) session-id))
                                         vec)
                  new-int-date (js-date->int-date (:day session))]
              (cond-> db
                (seq updated-sessions) (assoc-in [:sessions session-date] updated-sessions)
                (empty? updated-sessions) (update :sessions dissoc session-date)
                true (update-in [:sessions new-int-date] conj session)
                true (assoc-in [:selected :session] nil)))))

;; -- DELETE Session @ /api/sessions/:id --------------------------------------
(rf/reg-event-fx
 ::delete-session
 (fn-traced [db [_ id]]
            {:http-xhrio {:method          :delete
                          :uri             (endpoint "sessions" id)
                          :body            {}
                          :format          (ajax/json-request-format)
                          :response-format (ajax/json-response-format {:keywords? true})
                          :on-success      [::delete-session-success id]
                          :on-failure      [::api-request-error ::delete-session]}}))

(rf/reg-event-db
 ::delete-session-success
 (fn [db [_ id]]
   (let [session-date (->> db
                           :sessions
                           vals
                           flatten
                           (filter #(= (:session_id %) id))
                           first
                           :day
                           js-date->int-date)
         updated-sessions  (->> (get-in db [:sessions session-date])
                                (remove #(= (:session_id %) id))
                                vec)]
     (cond-> db
       (empty? updated-sessions) (update :sessions dissoc session-date)
       (seq updated-sessions) (assoc-in [:sessions session-date] updated-sessions)
       true (assoc-in [:selected :session] nil)))))

;; usage (dispatch [:api-request-error {:request-type <error-to-log-as>}])
(rf/reg-event-db
 ::api-request-error
 (fn [db [_ request-type response]]
   (-> db
       (assoc-in [:errors request-type] (get-in response [:response :errors])))))
