(ns dashboard.subs
  (:require
   [re-frame.core :as rf]))

(rf/reg-sub
 ::selected
 (fn [db [_ key]]
   (get-in db [:selected key])))

(rf/reg-sub
 ::dashboard-tab
 (fn [db]
   (:dashboard-tab db)))
