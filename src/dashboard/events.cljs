(ns dashboard.events
  (:require
   [re-frame.core :as rf]
   [utilities :as utils]))

(rf/reg-event-db
 ::set-selected-calendar-date
 (fn [db [_ js-calendar-date]]
   (let [clj-calendar-date (js->clj js-calendar-date)]
     (if-not (and clj-calendar-date (get clj-calendar-date "value"))
       (assoc-in db [:selected :calendar-date] nil)
       (assoc-in db [:selected :calendar-date]  (-> clj-calendar-date
                                                    (get "day")
                                                    utils/str-date->js-date
                                                    utils/js-date->int-date))))))

(rf/reg-event-db
 ::set-selected-calendar-filter
 (fn [db [_ id]]
   (-> db
       (assoc-in [:selected :calendar-date] nil)
       (assoc-in [:selected :calendar-filter] id))))

(rf/reg-event-db
 ::set-dashboard-tab
 (fn [db [_ tab]]
   (-> db
       (assoc-in [:selected :calendar-date] nil)
       (assoc-in [:selected :calendar-filter] "")
       (assoc :dashboard-tab tab))))