(ns dashboard.views
  (:require
   [reagent.core :as reagent]
   [re-frame.core :as rf]
   ["@nivo/calendar" :refer [ResponsiveCalendar]]
   ["@nivo/radar" :refer [ResponsiveRadar]]
   ["@nivo/pie" :refer [ResponsivePie]]
   [api.subs]
   [dashboard.events]
   [dashboard.subs]
   [utilities :as utils]))

(def weekdays ["Mon" "Tue" "Wed" "Thu" "Fri" "Sat" "Sun"])

(defn sum-activity-time
  "Sums the durations of the specified activity's sessions."
  [sessions activity-id]
  (->> sessions
       (filter #(= (:activity_id %) activity-id))
       (map #(utils/time-str->float (:duration %)))
       (reduce +)))

(defn activity-has-tag? [activities-map activity-id tag-id]
  (some #(= % tag-id) (-> activities-map
                          (get activity-id)
                          :tags)))

(defn sum-tag-time
  [sessions activities-map tag-id]
  (->> sessions
       (filter #(activity-has-tag? activities-map (:activity_id %) tag-id))
       (map #(utils/time-str->float (:duration %)))
       (reduce +)))

(defn get-date-duration-pp
  "Gets the duration for the specified date and creates a pretty print string for it."
  [sessions date]
  (->> sessions
       (filter #(= (:day %) date))
       first
       :duration
       utils/time-str->time-str-pp))

(defn nivo-calendar [data year]
  [:div {:style {:height 280}}
   [ResponsiveCalendar
    (clj->js
     {:from             (str year "-01-01")
      :to               (str year "-12-31")
      :emptyColor       "#eeeeee"
      :maxValue         5
      :valueFormat      ".1f"
      :onClick          #(rf/dispatch [::dashboard.events/set-selected-calendar-date %])
      :margin           {:top 40 :right 40 :bottom 40 :left 40}
      :theme            {:fontSize 18 :textColor "#bbbbbb"}
      :yearSpacing      40
      :monthBorderWidth 1
      :monthBorderColor "#bbbbbb"
      :dayBorderWidth   2
      :dayBorderColor   "#ffffff"
      :data             data})]])

(defn nivo-radar [keys data]
  [:div.column {:style {:height 500}}
   [ResponsiveRadar
    (clj->js
     {:keys keys
      :indexBy "name"
      :data data
      :colors {:scheme "nivo"}
      :fillOpacity 0.2
      :enableDots false
      :margin {:top 40 :right 40 :bottom 40 :left 40}
      :legends [{:anchor "top-left"
                 :direction "column"
                 :translateX 0
                 :translateY -40
                 :itemWidth 80
                 :itemHeight 20
                 :itemTextColor "#333333"
                 :symbolSize 12
                 :symbolShape "circle"}]})]])

(defn nivo-pie [data tooltip]
  [:div.column {:style {:height 500}}
   [ResponsivePie
    (clj->js
     {:data data
      :margin {:top 40 :right 40 :bottom 40 :left 40}
      :innerRadius 0.5
      :tooltip #(reagent/as-element [tooltip %])
      :padAngle 0.7
      :corderRadius 3
      :activeOuterRadiusOffset 8
      :borderWidth 1
      :colors {:scheme "nivo"}
      :borderColor {:from "color" :modifiers [["darker" 0.2]]}})]])

(defn find-activity-id [activity-name activities-map]
  (->> activities-map
       vals
       (filter #(= (:name %) activity-name))
       first
       :activity_id))

(defn nivo-pie-element->activity-id [nivo-pie-element]
  (let [activities-map @(rf/subscribe [::api.subs/activities])]
    (-> nivo-pie-element
        js->clj
        (get "datum")
        (get "id")
        str
        (find-activity-id activities-map))))

(defn days-duration [activity-id js-start-date js-end-date]
  (let [sessions-map @(rf/subscribe [::api.subs/sessions])
        valid-sessions (->> (range (utils/js-date->int-date js-start-date)
                                   (inc (utils/js-date->int-date js-end-date)))
                            (map #(get sessions-map %))
                            flatten
                            (filter #(= (:activity_id %) activity-id)))
        number-of-days (inc (utils/date-diff js-start-date js-end-date))]
    (->> (range 0 number-of-days)
         (map #(utils/add-days-to-date js-start-date %))
         (map #(get-date-duration-pp valid-sessions %)))))

(defn week-pie-tooltip [nivo-pie-element]
  (let [activity-id (nivo-pie-element->activity-id nivo-pie-element)
        {:keys [start-date end-date]} (utils/start-end-of-week (js/Date.))
        weekdays-duration (days-duration activity-id start-date end-date)]
    [:div.box
     [:table.table
      [:thead
       [:tr
        (for [day weekdays]
          ^{:key day} [:th.has-text-centered.has-background-info.has-text-white day])]]
      [:tbody
       (into
        [:tr]
        (map
         #(vec [:td.has-text-centered.has-text-grey-dark (if (= % "") "-" %)])
         weekdays-duration))]]]))

(defn month-pie-tooltip [nivo-pie-element]
  (let [today (js/Date.)
        activity-id (nivo-pie-element->activity-id nivo-pie-element)
        {:keys [:start-date :end-date]} (utils/start-end-of-month today)
        start-date-offset (dec (.getDay start-date))
        calendar-days (->> (range 1 (inc (.getDate end-date)))
                           (concat (repeat start-date-offset ""))
                           (partition 7 7 (repeat 7 "")))
        calendar-days-duration (->> (days-duration activity-id start-date end-date)
                                    (concat (repeat start-date-offset  ""))
                                    (partition 7 7 (repeat 7 "")))
        merged-calendar (map #(vec [%1 %2]) calendar-days calendar-days-duration)
        create-calendar-day (fn [day duration]
                              [:td
                               [:div.container
                                [:div.columns.is-centered
                                 [:div.column.is-narrow
                                  [:table.table
                                   [:thead
                                    [:tr
                                     [:th.has-text-centered
                                      (when (= (.getDate today) day)
                                        {:class "has-background-info has-text-white"}) day]]]
                                   [:tbody
                                    [:tr
                                     [:td.has-text-centered.has-text-grey-dark
                                      (if-not (= duration "") (str duration) "-")]]]]]]]])]
    [:div.box
     [:table.table
      [:thead
       [:tr
        (for [day weekdays]
          ^{:key day} [:th.has-text-centered.has-background-info.has-text-white day])]]
      (into
       [:tbody]
       (map
        #(into [:tr] (map create-calendar-day (first %) (last %)))
        merged-calendar))]]))

(defn year-analysis-data [sessions-map]
  (let [total-day-duration (fn [day]
                             (->> (get sessions-map day)
                                  (map :duration)
                                  (reduce (fn [acc item]
                                            (+ acc (utils/time-str->float item))) 0)))]
    (->> sessions-map
         keys
         (map #(assoc {}
                      :day (utils/int-date->str-date %)
                      :value (total-day-duration %))))))

(defn week-radar [week-data]
  (let [radar-data (->> week-data
                        (filter #(or (pos? (:current-week-time %))
                                     (pos? (:previous-week-time %))))
                        (map #(into {} {"name" (:name %)
                                        "Current Week" (:current-week-time %)
                                        "Previous Week" (:previous-week-time %)})))]
    [nivo-radar ["Previous Week" "Current Week"] radar-data]))

(defn week-pie [week-data]
  (let [pie-data (->> week-data
                      (filter #(pos? (:current-week-time %)))
                      (map #(into {} {"id" (:name %)
                                      "label" (:name %)
                                      "value" (:current-week-time %)})))]
    [nivo-pie pie-data week-pie-tooltip]))

(defn month-radar [month-data]
  (let [radar-data (->> month-data
                        (filter #(or (pos? (:current-month-time %))
                                     (pos? (:previous-month-time %))))
                        (map #(into {} {"name" (:name %)
                                        "Current Month" (:current-month-time %)
                                        "Previous Month" (:previous-month-time %)})))]
    [nivo-radar ["Previous Month" "Current Month"] radar-data]))

(defn month-pie [month-data]
  (let [pie-data (->> month-data
                      (filter #(pos? (:current-month-time %)))
                      (map #(into {} {"id" (:name %)
                                      "label" (:name %)
                                      "value" (:current-month-time %)})))]
    [nivo-pie pie-data month-pie-tooltip]))

(defn week-sessions [js-date sessions-map]
  (let [week (-> js-date
                 utils/start-end-of-week
                 (update :start-date utils/js-date->int-date)
                 (update :end-date utils/js-date->int-date))]
    (->> (range (:start-date week) (inc (:end-date week)))
         (map #(get sessions-map %))
         (filter some?)
         flatten)))

(defn month-sessions [month year sessions-map]
  (let [start-date (+ (* year 10000) (* month 100) 1)]
    (->> (range start-date (+ start-date 31))
         (map #(get sessions-map %))
         (filter some?)
         flatten)))

(defn current-previous-week-tag-data [sessions-map activities-map tags-map]
  (let [today (js/Date.)
        current-week-sessions (week-sessions today sessions-map)
        previous-week-sessions (week-sessions (utils/add-days-to-date today -7) sessions-map)]
    (->> (vals tags-map)
         (map #(assoc {}
                      :tag_id (:tag_id %)
                      :name (:name %)
                      :current-week-time (sum-tag-time current-week-sessions activities-map (:tag_id %))
                      :previous-week-time (sum-tag-time previous-week-sessions activities-map (:tag_id %)))))))

(defn current-previous-week-activity-data [sessions-map activities-map]
  (let [today (js/Date.)
        current-week-sessions (week-sessions today sessions-map)
        previous-week-sessions (week-sessions (utils/add-days-to-date today -7) sessions-map)]
    (->> (vals activities-map)
         (map #(assoc {}
                      :activity_id (:activity_id %)
                      :name (:name %)
                      :current-week-time (sum-activity-time current-week-sessions (:activity_id %))
                      :previous-week-time (sum-activity-time previous-week-sessions (:activity_id %)))))))

(defn current-previous-month-tag-data [sessions-map activities-map tags-map month year]
  (let [[previous-month previous-year] (if (= month 1)
                                         [12 (dec year)]
                                         [(dec month) year])
        current-month-sessions (month-sessions month year sessions-map)
        previous-month-sessions (month-sessions previous-month previous-year sessions-map)]
    (->> (vals tags-map)
         (map #(assoc {}
                      :tag_id (:tag_id %)
                      :name (:name %)
                      :current-month-time (sum-tag-time current-month-sessions activities-map (:tag_id %))
                      :previous-month-time (sum-tag-time previous-month-sessions activities-map (:tag_id %)))))))

(defn current-previous-month-activity-data [sessions-map activities-map month year]
  (let [[previous-month previous-year] (if (= month 1)
                                         [12 (dec year)]
                                         [(dec month) year])
        current-month-sessions (month-sessions month year sessions-map)
        previous-month-sessions (month-sessions previous-month previous-year sessions-map)]
    (->> (vals activities-map)
         (map #(assoc {}
                      :activity_id (:activity_id %)
                      :name (:name %)
                      :current-month-time (sum-activity-time current-month-sessions (:activity_id %))
                      :previous-month-time (sum-activity-time previous-month-sessions (:activity_id %)))))))

(defn daily-activities-duration
  [activities sessions int-date]
  (let [date-sessions (->> (get sessions int-date)
                           (map #(assoc {}
                                        :id (:session_id %)
                                        :duration (:duration %)
                                        :name (->> % :activity_id (get activities) :name))))]
    [:div.box
     [:div.columns
      [:div.column]
      [:div.column.is-narrow
       [:button.delete.is-medium
        {:on-click #(rf/dispatch [::dashboard.events/set-selected-calendar-date nil])}]]]
     (for [session date-sessions]
       [:article.message.is-info {:key (:id session)}
        [:div.message-body
         [:b (utils/time-str->time-str-pp (:duration session))]
         (:name session)]])]))

(defn activity-week-panel [week-data]
  [:div.container
   [:h4.subtitle.is-4 "Week Analysis"]
   [:div.columns
    [week-radar week-data]
    [week-pie week-data]]])

(defn activity-month-panel [month-data]
  [:div.container
   [:h4.subtitle.is-4 "Month Analysis"]
   [:div.columns
    [month-radar month-data]
    [month-pie month-data]]])

(defn tag-week-panel [week-data]
  [:div.container
   [:h4.subtitle.is-4 "Week Analysis"]
   [:div.columns
    [week-radar week-data]]])

(defn tag-month-panel [month-data]
  [:div.container
   [:h4.subtitle.is-4 "Month Analysis"]
   [:div.columns
    [month-radar month-data]]])

(defn filter-sessions [pred sessions-map]
  (->> sessions-map
       vals
       flatten
       (filter pred)
       (reduce (fn [acc item]
                 (let [updated-item (update item :day utils/str-date->js-date)
                       int-date (utils/js-date->int-date (:day updated-item))]
                   (assoc! acc int-date (conj (get acc int-date []) updated-item))))
               (transient {}))
       (persistent!)))

(defn year-calendar [sessions]
  (let [activities-map @(rf/subscribe [::api.subs/activities])
        selected-calendar-date @(rf/subscribe [::dashboard.subs/selected :calendar-date])]
    [:div
     [nivo-calendar (year-analysis-data sessions) 2023]
     (when selected-calendar-date
       [daily-activities-duration activities-map sessions selected-calendar-date])]))


(defn calendar-dropdown [values id-fn name-fn]
  (let [sorted-values (sort-by name-fn values)]
    [:div.select
     [:select
      {:defaultValue ""
       :on-change #(rf/dispatch  [::dashboard.events/set-selected-calendar-filter (-> % .-target .-value)])}
      [:option {:hidden false :disabled false :value ""} "All"]
      (for [value sorted-values]
        [:option {:value (id-fn value) :key (id-fn value)} (name-fn value)])]]))

(defn activity-year-panel [sessions-map activities-map]
  (let [selected-value @(rf/subscribe [::dashboard.subs/selected :calendar-filter])
        filtered-sessions (if (not-empty selected-value)
                            (filter-sessions #(= (:activity_id %) (js/parseInt selected-value))
                                             sessions-map)
                            sessions-map)]
    [:div.container
     [calendar-dropdown (vals activities-map) :activity_id :name]
     [year-calendar filtered-sessions]]))

(defn tag-year-panel [sessions-map activities-map tags-map]
  (let [selected-value @(rf/subscribe [::dashboard.subs/selected :calendar-filter])
        filter-pred (fn [session tag-id]
                      (activity-has-tag? activities-map (:activity_id session) tag-id))
        filtered-sessions (if (not-empty selected-value)
                            (filter-sessions #(filter-pred % (js/parseInt selected-value))
                                             sessions-map)
                            sessions-map)]
    [:div.container
     [calendar-dropdown (vals tags-map) :tag_id :name]
     [year-calendar filtered-sessions]]))


(defn activity-panels [sessions-map activities-map]
  (let [current-month (inc (.getMonth (js/Date.)))
        current-year (.getFullYear (js/Date.))]
    [:div
     [activity-year-panel sessions-map activities-map]
     [:hr.is-divider]
     [activity-week-panel (current-previous-week-activity-data sessions-map activities-map)]
     [:hr.is-divider]
     [activity-month-panel (current-previous-month-activity-data sessions-map activities-map
                                                                 current-month current-year)]]))

(defn tag-panels [sessions-map activities-map tags-map]
  (let [current-month (inc (.getMonth (js/Date.)))
        current-year (.getFullYear (js/Date.))]
    [:div
     [tag-year-panel sessions-map activities-map tags-map]
     [:hr.is-divider]
     [tag-week-panel (current-previous-week-tag-data sessions-map activities-map tags-map)]
     [:hr.is-divider]
     [tag-month-panel (current-previous-month-tag-data sessions-map activities-map
                                                       tags-map current-month current-year)]]))

(defn dashboard-panel []
  (let [sessions-map @(rf/subscribe [::api.subs/sessions])
        activities-map @(rf/subscribe [::api.subs/activities])
        tags-map @(rf/subscribe [::api.subs/tags])
        current-tab @(rf/subscribe [::dashboard.subs/dashboard-tab])]
    [:div
     [:div.tabs.is-centered.is-toggle
      [:ul
       [:li {:class (when (= current-tab :activities) "is-active")}
        [:a {:on-click #(rf/dispatch [::dashboard.events/set-dashboard-tab :activities])}
         "Activities"]]
       [:li {:class (when (= current-tab :tags) "is-active")}
        [:a {:on-click #(rf/dispatch [::dashboard.events/set-dashboard-tab :tags])}
         "Tags"]]]]
     (case current-tab
       :activities [activity-panels sessions-map activities-map]
       :tags [tag-panels sessions-map activities-map tags-map])]))
